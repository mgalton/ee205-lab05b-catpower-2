###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Mariko Galton <mgalton@hawaii.edu>
### @date 17_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = g++

CFLAGS = -g -Wall -Wextra

TARGET = catPower

all: $(TARGET)

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp

foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp

gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp

catpower.o: catpower.cpp catpower.h
	$(CC) $(CFLAGS) -c catpower.cpp

catPower.o: catPower.cpp ev.h megaton.h foe.h gge.h catpower.h 
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o ev.o megaton.o foe.o gge.o catpower.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o megaton.o foe.o gge.o catpower.o

clean:
	rm -f $(TARGET) *.o
